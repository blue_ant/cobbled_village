/*=require ./includes/blocks/InteractiveForm.js */
/*=require ./includes/blocks/Preloader.js */

const elHTML = document.documentElement;
const mainBlock = document.querySelector('main');
const footerBlock = [].slice.call(document.querySelectorAll('.Footer'));
const header = document.querySelector('.Header');
const burger = document.querySelector('.Header_burger');
const textBurger = document.querySelector('.Header_burgerText');
const textBurgerTrue = textBurger.innerHTML
const menuMob = document.querySelector('.MenuMobile');

const preloader = document.getElementById('pagePreloader')
window.pagePreloader = new Preloader(preloader);
window.pagePreloader.hide();

burger.onclick = () => {
    burger.classList.toggle('Header_burger-close');
    header.classList.toggle('Header-darkText');
    menuMob.classList.toggle('MenuMobile-open');
    document.documentElement.classList.toggle('disabled');

    if (textBurgerTrue === textBurger.innerHTML) {
        textBurger.innerHTML = 'Закрыть';
    } else {
        textBurger.innerHTML = textBurgerTrue;
    }
}

if (window.location.pathname === '/') {
    if (elHTML.clientWidth >= 1024) {
        document.body.classList.add('disabled');
    }
};

if (window.location.pathname !== '/') {
  localStorage.setItem('fullPageActiveSlide', '0');
};

window.addEventListener('resize', () => {
    if (window.location.pathname === '/') {
        if (elHTML.clientWidth >= 1024) {
            document.body.classList.add('disabled');
        } else {
            document.body.classList.remove('disabled');
        }
    };
});

document.addEventListener("DOMContentLoaded", () => {
    if (window.location.pathname === '/') {
        mainBlock.style.height = '100%';
        mainBlock.style.paddingTop = `0`;
        mainBlock.classList.add('not-flex');
        header.classList.remove('Header-white');

        footerBlock.forEach((el) => {
            if (el.previousElementSibling === mainBlock) {
                el.style.display = "none"
            }
        });
    }
});

[].forEach.call(document.querySelectorAll('.form'), form => {
    new InteractiveForm(form);
});

function parseQueryString(string) {
    if (string === "" || string == null) return {};
    if (string.charAt(0) === "?") string = string.slice(1);
    var entries = string.split("&"),
        data0 = {},
        counters = {};
    for (var i = 0; i < entries.length; i++) {
        var entry = entries[i].split("=");
        var key5 = decodeURIComponent(entry[0]);
        var value = entry.length === 2 ? decodeURIComponent(entry[1]) : "";
        if (value === "true") value = true;
        else if (value === "false") value = false;
        var levels = key5.split(/\]\[?|\[/);
        var cursor = data0;
        if (key5.indexOf("[") > -1) levels.pop();
        for (var j = 0; j < levels.length; j++) {
            var level = levels[j],
                nextLevel = levels[j + 1];
            var isNumber = nextLevel == "" || !isNaN(parseInt(nextLevel, 10));
            var isValue = j === levels.length - 1;
            if (level === "") {
                var key5 = levels.slice(0, j).join();
                if (counters[key5] == null) counters[key5] = 0;
                level = counters[key5]++;
            }
            if (cursor[level] == null) {
                cursor[level] = isValue ? value : isNumber ? [] : {};
            }
            cursor = cursor[level];
        }
    }
    return data0;
}

if (header && window.location.pathname === '/') eventsScroll();

function eventsScroll() {
  checkScroll();

  window.addEventListener('resize', () => {
    checkScroll();
  });
  window.addEventListener('scroll', () => {
    checkScroll();
  });

  function checkScroll() {
    const scroll = window.pageYOffset;

    if (window.innerWidth < 1024) {
      if (scroll < 50) {
        header.classList.remove('Header-white-m');
      } else {
        header.classList.add('Header-white-m');
      }
    }
  }
}

$('[data-custom-select]').each(function(){
    var $this = $(this),
        numberOfOptions = $(this).children('option').length;

    $this.addClass('select-hidden');
    $this.wrap('<div class="select"></div>');
    $this.after('<div class="select-styled"></div>');

    var $styledSelect = $this.next('div.select-styled');
    $styledSelect.text($this.children('option').eq(0).text());

    var $list = $('<ul />', {
        'class': 'select-options'
    }).insertAfter($styledSelect);

    for (var i = 0; i < numberOfOptions; i++) {
        $('<li />', {
            text: $this.children('option').eq(i).text(),
            rel: $this.children('option').eq(i).val()
        }).appendTo($list);
    }

    var $listItems = $list.children('li');

    $styledSelect.click(function(e) {
        e.stopPropagation();
        $('div.select-styled.active').not(this).each(function(){
            $(this).removeClass('active').next('ul.select-options').hide();
        });
        $(this).toggleClass('active').next('ul.select-options').toggle();
    });

    $listItems.click(function(e) {
        e.stopPropagation();
        $styledSelect.text($(this).text()).removeClass('active');
        $this.val($(this).attr('rel'));
        $list.hide();
        //console.log($this.val());
    });

    $(document).click(function() {
        $styledSelect.removeClass('active');
        $list.hide();
    });

});

let consultForm = $('.popupConsult__form');

new InteractiveForm(consultForm);

consultForm.on('change', 'input', function() {
    let validateForm = $( consultForm ).validate();
    let formIsValid = validateForm.form();

    if (formIsValid) {
        consultForm.find('button[type="submit"]').addClass('valid');
    } else {
        consultForm.find('button[type="submit"]').removeClass('valid');
    }
});

$('[data-head]').on('click', function () {
   let subtitle = $(this).data('head');
   let comment = $(this).data('comment');
   let popupConsult = $('#popup-consult');
   let inputComment = popupConsult.find('input[name="comment"]');
   popupConsult.find('.popupConsult__subtitle').html(subtitle);
   inputComment.val(comment);
});
