let $tooltip = $(".section__label");

$(".section__svg-hover")
  .click(function(event) {
    event.preventDefault();
    let $link = $(event.currentTarget);
    if ($link.data("status") !== "sold" && $link.data("status") !== "booked") {

      if ($(window).width() >= 1280 || $tooltip.hasClass('section__label--active')) {
        var link = $(this).data('url');
        window.location = link;
      }
    }
  })
  .hover(
    function(/*event*/) {
      setTimeout(() => {
        var id = $(this).data("id");
        fillLabel($(this));

        $(`.section__svg-hover-back[data-id=${id}]`).addClass("section__svg-hover-back--active");
      }, 1);
    },
    function() {
      $(".section__svg-hover-back").removeClass("section__svg-hover-back--active");
      $tooltip.removeClass("section__label--active");
    }
  );

function checkWindth() {
  let width = $("svg.section__svg").width();
  $("img.section__svg").css({ width: `${width}px` });
}

checkWindth();
$(window).resize(_.debounce(checkWindth, 100));

function fillLabel(item) {
  let status = item.data("status");

  if (status === "sold") {
    $tooltip
      .html(
        `
<div class="section__label-description">
  <p class="section__label--single-title-text">Квартира продана</p>
</div>`
      )
      .addClass("section__label--active");
  } else if (status === "booked") {
    $tooltip
      .html(
        `
<div class="section__label-description">
  <p class="section__label--single-title-text">Квартира забронирована</p>
</div>`
      )
      .addClass("section__label--active");
  } else {
    let number = item.data("number");
    let area = item.data("area");
    let rooms = item.data("rooms");
    let price = item.data("price");

    $tooltip
      .html(
        `
<div class="section__label-description">
  <div class="section__label-text section__label-text--number">
    Квартира <p>№</p>
    <span>${number}</span>
  </div>
  <div class="section__label-text section__label-text--area"><span>${area}</span> м² </div>
  <div class="section__label-text section__label-text--rooms"><span>${rooms}</span> комн.</div>
  <div class="section__label-text section__label-text--price"><span>${price}</span> ₽</div>
</div>`
      )
      .addClass("section__label--active");
  }
}

$(".section__visual-wrap").scroll(function() {
  $tooltip.removeClass("section__label--active");
});

$(".section__content").mousemove(function(e) {
  window.requestAnimationFrame(() => {
    const pos = $(this).offset();
    const elem_left = pos.left;
    const elem_top = pos.top;
    const x = e.pageX - elem_left;
    const y = e.pageY - elem_top - 108;

    const minLeft = ($tooltip.outerWidth() / 2) + 8;
    const maxLeft = $(window).width() - ($tooltip.outerWidth() / 2) - 8;

    if (x < minLeft) {
      $tooltip.css({ top: y + "px", left: minLeft + "px" });
      $tooltip.removeClass('section__label--right');
      $tooltip.addClass('section__label--left');
    } else if (x > maxLeft) {
      $tooltip.css({ top: y + "px", left: maxLeft + "px" });
      $tooltip.removeClass('section__label--left');
      $tooltip.addClass('section__label--right');
    } else {
      $tooltip.css({ top: y + "px", left: x + "px" });
      $tooltip.removeClass('section__label--left');
      $tooltip.removeClass('section__label--right');
    }
  });
});
