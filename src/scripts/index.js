/*=require0 ./includes/blocks/Map.js*/
const paginationFullPage = document.querySelector('.indexFullPage_pagination');
const sliderPark = document.querySelector('.indexContainer_sliderPark');
const sliderParkSlides = [].slice.call(document.querySelectorAll('.indexContainer_sliderPark .swiper-slide'));
const btnSection = document.querySelector('.indexName_btnDown');
const mapContainer = document.getElementById('map');
const scrollSwiper = document.querySelector('.indexFullPage .swiper-wrapper');
const elSliderPark = document.querySelector('.indexContainer_sliderPark');
let fullPageIndex = undefined;
let parentSliderPark = sliderPark;
let count = document.querySelector('.indexContainer_count');
let progressSlide = document.querySelector('.indexContainer_btnProgresValue');
let sectionsAttribut = [].slice.call(document.querySelectorAll('section[data-name]'));
let map = undefined;

const enabledSwiper = () => {
	fullPageIndex = new Swiper('.indexFullPage', {
		slidesPerView: "auto",
		direction: "vertical",
		autoHeight: true,
		mousewheel: true,
		noSwiping: true,
		noSwipingClass: 'indexFullPage',
		pagination: {
			el: '.indexFullPage_pagination'
		},
		navigation: {
			nextEl: '.indexName_btnDown',
		}
	});

	fullPageIndex.update();

	fullPageIndex.on("slideChangeTransitionStart", () => {
		if (fullPageIndex.activeIndex === 0) {
			paginationFullPage.classList.remove('indexFullPage_pagination-whiteOpacity')
			paginationFullPage.classList.remove('indexFullPage_pagination-black')
		} else if (fullPageIndex.activeIndex % 2 !== 0) {
			paginationFullPage.classList.remove('indexFullPage_pagination-whiteOpacity')
			paginationFullPage.classList.add('indexFullPage_pagination-black')
		} else if (fullPageIndex.activeIndex % 2 === 0) {
			paginationFullPage.classList.add('indexFullPage_pagination-whiteOpacity')
			paginationFullPage.classList.remove('indexFullPage_pagination-black')
		}
	});

	fullPageIndex.on("slideChangeTransitionEnd", () => {
		if (parentSliderPark.classList.contains('swiper-slide-active')) {
			if (parkSlider.activeIndex !== sliderParkSlides.length - 1) {
				parkSlider.autoplay.start();
				progressSlide.classList.add('indexContainer_btnProgresValue-animation');
			}
		} else {
			parkSlider.autoplay.stop();
			progressSlide.classList.remove('indexContainer_btnProgresValue-animation');
		}
	});

	fullPageIndex.on("transitionEnd", () => {
		if (scrollSwiper.getBoundingClientRect().top < 0) {
			header.classList.add('Header-white');
		} else if (scrollSwiper.getBoundingClientRect().top >= 0) {
			header.classList.remove('Header-white');
		}
	});

	fullPageIndex.on("slideChange", () => {
		const activeSlide = fullPageIndex.activeIndex;
		localStorage.setItem('fullPageActiveSlide', activeSlide);
	});

	function changeActiveSlide() {
		const index = localStorage.getItem('fullPageActiveSlide');
		if (index) fullPageIndex.slideTo(+index, 0, false);
	}

	setTimeout(() => {
		changeActiveSlide();
	}, 0);
};

const initSwiperMainVideoBtns = () => {
    new Swiper('.SwiperMainVideoBtns', {
		loop: true,
		pagination: {
	    el: '.SwiperMainVideoBtns .swiper-pagination',
	    type: 'bullets',
	  }
	});
};

window.onload = () => {
    if (elHTML.clientWidth >= 1024) {
        enabledSwiper();
    }
    initSwiperMainVideoBtns();
};

window.addEventListener('resize', () => {
	if (elHTML.clientWidth < 1024) {
		if (fullPageIndex !== undefined) {
			fullPageIndex.destroy(false, true);
		}
	} else if (fullPageIndex === undefined) {
		enabledSwiper();
	} else if (elHTML.clientWidth >= 1024 && fullPageIndex.destroyed === true) {
		enabledSwiper();
	}
});

const parkSlider = new Swiper(elSliderPark, {
	slidesPerView: 1,
	navigation: {
		nextEl: '.indexContainer_btnNext',
		prevEl: '.indexContainer_btnPrev',
	},
	loop: true,
	autoplay: {
		delay: 5000,
		stopOnLastSlide: true,
		disableOnInteraction: false,
	},
});

parkSlider.autoplay.stop();

while (parentSliderPark.classList.contains('swiper-slide') !== true) {
	parentSliderPark = parentSliderPark.parentNode
}

const countChange = () => {
	let index = parkSlider.realIndex + 1;

	if (index <= 9) {
		count.innerHTML = `0${index}`;
	} else {
		count.innerHTML = `${index}`;
	}
}

parkSlider.on('slideChangeTransitionStart', () => {
	countChange();
	progressSlide.classList.remove('indexContainer_btnProgresValue-animation');
});

parkSlider.on('slideChangeTransitionEnd', () => {
	if (parkSlider.isEnd) {
		progressSlide.classList.remove('indexContainer_btnProgresValue-animation');
	} else {
		progressSlide.classList.add('indexContainer_btnProgresValue-animation');
	}
});

elSliderPark.addEventListener('mouseover', () => {
	parkSlider.autoplay.stop();
});

elSliderPark.addEventListener('mouseout', () => {
	parkSlider.autoplay.start();
});

parkSlider.on('autoplayStart', () => {
	progressSlide.classList.remove('indexContainer_btnProgresValue-animation');

	setTimeout(() => {
		progressSlide.classList.remove('indexContainer_btnProgresValue-pause');
		progressSlide.classList.add('indexContainer_btnProgresValue-animation');
	}, 0);
});

parkSlider.on('autoplayStop', () => {
	progressSlide.classList.add('indexContainer_btnProgresValue-pause');
});

btnSection.addEventListener('click', () => {
	if (elHTML.clientWidth < 1024) {
		sectionsAttribut.forEach(element => {
			if (element.getAttribute('data-name') === 'park') {
				element.scrollIntoView({ block: "start", behavior: "smooth" })
			}
		});
	}
});

if (map !== null) {
	map = new Map('indexMap', 15, mapContainer, 'data-json');
}
