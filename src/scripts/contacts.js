/*=require0 ./includes/blocks/Map.js*/
const tabs = [].slice.call(document.querySelectorAll('.contacts_tab'));
const mapContainer = document.getElementById('map');
const contents = [].slice.call(document.querySelectorAll('.contacts_content'));
let map = undefined;

const toggleActiveClass = (block, attr) => {
    for (let i = 0; i < block.length; i++) {
        if (block[i].classList.contains(attr)) {
            block[i].classList.remove(attr);
        } else {
            block[i].classList.add(attr);
        }
    }
}

tabs.forEach((element, i) => {
    element.addEventListener("click", () => {
        if (element.classList.contains("contacts_tab-active") === false) {
            toggleActiveClass(tabs, 'contacts_tab-active');
            toggleActiveClass(contents, 'contacts_content-active');

            element.classList.add("contacts_tab-active");

            if (i === 0) {
                element.parentNode.classList.remove('contacts_tabs-second');
            } else {
                element.parentNode.classList.add('contacts_tabs-second');
            }
        }
    });
});

if (mapContainer !== null) {
    map = new Map('contactMap', 17, mapContainer);
    setTimeout(function() {
        map.contacts();
    }, 1000);
}
