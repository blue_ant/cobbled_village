genplan();

function genplan() {
  var loadImage = function(src, callback) {
    var img = new Image();
    img.onload = img.onerror = function(evt) {
      callback(evt.type == 'error', img);
      img.onload = img.onerror = img = null;
    };
    img.src = src;
  };

  $('.genplan__object').hover(function(e) {
    $(`.genplan__render[data-id=${this.dataset.id}]`).addClass('genplan__render--active');
    $(`.genplan__label[data-id=${this.dataset.id}]`).addClass('genplan__label--active');
  }, function() {
    $(`.genplan__render[data-id=${this.dataset.id}]`).removeClass('genplan__render--active');
    $(`.genplan__label[data-id=${this.dataset.id}]`).removeClass('genplan__label--active');
  });

  $('.genplan__label').hover(function(e) {
    $(`.genplan__render[data-id=${this.dataset.id}]`).addClass('genplan__render--active');
  }, function() {
    $(`.genplan__render[data-id=${this.dataset.id}]`).removeClass('genplan__render--active');
  });

  $('.genplan__object').click(function() {
    var link = $(this).data('url');
    window.location = link;
  });

  loadImage($('.genplan__img-common').attr('src'), function(err, img) {
    if (err) {
      alert('image not load');
    } else {
      var ratio = img.width / img.height;
      var windowWidth = $(window).width();

      updateGenplan(ratio, windowWidth);

      $(window).resize(() => {
        windowWidth = $(window).width();
        updateGenplan(ratio, windowWidth);
      });
    }
  });

  function updateGenplan(ratio, windowWidth) {
    $('.genplan__content').css({'height': '', 'margin-top': '', 'margin-bottom': '', 'margin-left': ''});
    $('.genplan__img-common').css({'height': '', 'width': ''});

    if (windowWidth >= 768) resizeImg(ratio);
  }

  function resizeImg(ratio) {
    const $genplan = $('.genplan');
    const $contentWrap = $genplan.find('.genplan__content-wrap');
    const $content = $genplan.find('.genplan__content');
    const $img = $genplan.find('.genplan__img-common');
    let $hGenplan = $genplan.outerHeight();
    let $hContentWrap = $contentWrap.outerHeight();

    if ($hGenplan > $hContentWrap) {
      $content.css({'margin-top': '0', 'margin-bottom': '0'});

      $hGenplan = $genplan.outerHeight();
      $hContentWrap = $contentWrap.outerHeight();


      if ($hGenplan > $hContentWrap) {
        const imgWidth = $hGenplan * ratio;
        const genWidth = $genplan.width();
        $content.css({'height': `${$hGenplan}px`});
        $img.css({'height': '100%', 'width': `${imgWidth}px`});

        $content.css({'margin-left': `${-(imgWidth - genWidth) * 0.4}px`});
      }
    }
  }
}
