const gallerySlider = new Swiper(".gallery_single_slider", {
    loop: true,
    loopedSlides: 12,
    grabCursor: true,
    pagination: {
        el: '.swiper-pagination',
        type: 'custom',
        renderCustom: function(gallerySlider, current, total) {
            return `<span>${current} </span> из ${total}`;
        },
    },
    navigation: {
        nextEl: ".gallery_single_btn-next",
        prevEl: ".gallery_single_btn-prev"
    }
});