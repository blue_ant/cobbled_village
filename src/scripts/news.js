const scrollContentDetail = document.querySelector('.news_detail');
const scrollContentList = document.querySelector('.news_list');

const listNews = new PerfectScrollbar(scrollContentList);
const detailNews = new PerfectScrollbar(scrollContentDetail);

const newsList = [].slice.call(document.querySelectorAll('.news_item'));
const newsDetail = [].slice.call(document.querySelectorAll('.news_detailItem'));
const newsDetailPopup = [].slice.call(document.querySelectorAll('.popupNews'));

const titleNews = [].slice.call(document.querySelectorAll('.news_headline'));
const descNews = [].slice.call(document.querySelectorAll('.news_text'));

const lengthStr = (str, numLength) => {
    let newStr = '';

    if (str.length > numLength) {
        return newStr = `${str.substring(0, numLength)}...`;
    }

    return str;
}

titleNews.forEach(el => {
    let str = el.innerHTML;
    el.innerHTML = lengthStr(str, 50);
});

descNews.forEach(el => {
    let str = el.innerHTML;
    el.innerHTML = lengthStr(str, 180);
});

const searhDetail = (el) => {
    for (let i = 0; i < newsDetail.length; i++) {
        if (el.getAttribute('data-open') === newsDetail[i].getAttribute('data-open')) {
            newsDetail[i].classList.add('news_detailItem-active');
        }
    }
}

newsList.forEach((element) => {
    if (element.classList.contains('news_item-active')) {
        searhDetail(element);
    }

    element.addEventListener('click', () => {
        if (elHTML.clientWidth < 1024) {
            for (let i = 0; i < newsDetailPopup.length; i++) {
                if (element.getAttribute('data-src') === newsDetailPopup[i].getAttribute('id')) {
                    $.fancybox.open(newsDetailPopup[i])
                }
            }
        }

        for (let i = 0; i < newsList.length; i++) {
            if (newsList[i].classList.contains('news_item-active')) {
                newsList[i].classList.remove('news_item-active');
            }
        }

        for (let i = 0; i < newsDetail.length; i++) {
            if (newsDetail[i].classList.contains('news_detailItem-active')) {
                newsDetail[i].classList.remove('news_detailItem-active');
            }
        }

        element.classList.add('news_item-active');
        searhDetail(element);
    });

    window.addEventListener('resize', () => {
        if (elHTML.clientWidth > 1024) {
            for (let i = 0; i < newsDetailPopup.length; i++) {
                if (newsDetailPopup[i].classList.contains('fancybox-content')) {
                    $.fancybox.close(newsDetailPopup[i]);
                }
            }
        }
    });
});
