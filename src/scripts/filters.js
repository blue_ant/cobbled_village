_.templateSettings.interpolate = /{{([\s\S]+?)}}/g;
class TypeItemList {
  constructor(data) {
    this.template = `<a class="filters-flats__item" href="{{href}}">
      <% if (discount) { %>
      <div class="filters-flats__discount">
        <svg width="17" height="27">
            <use xlink:href="#discount"></use>
        </svg>
      </div>
      <% } %>
      <div class="filters-flats__img" style="background-image: url({{pic}})"></div>
      <div class="filters-flats__description-wrap">
        <div class="filters-flats__description">
            <p class="filters-flats__price">от {{price}} ₽</p>
            <p class="filters-flats__area">от {{area}} м²</p>
        </div>
        <p class="filters-flats__rooms">{{rooms}} комн</p>
      </div>
    </a>`;
    this.data = data;
    this.renderFn = _.template(this.template);
  }
  render() {
    return this.renderFn(this.data);
  }
}

class Paginations {
  constructor(el, data) {
    this.template = `<a class="filters-pag__arrow filters-pag__arrow--prev js-nav" data-page="{{prev}}"><svg width="9" height="15"><use xlink:href="#arrow-prev"></use></svg></a>
      <div class="filters-pag__items">
        <% items.forEach(function(item) { %>
        <a class="filters-pag__item js-nav {{item.active ? 'filters-pag__item--active': ''}}" data-page="{{item.offset}}"><span>{{item.title}}</span></a>
        <% })%>
      </div>
      <a class="filters-pag__arrow filters-pag__arrow--next js-nav" data-page="{{next}}"><svg width="9" height="15"> <use xlink:href="#arrow-next"></use></svg></a>`;
    this.parent = el;
    this.data = data;
    this.renderFn = _.template(this.template);
  }
  calculateDataRender() {
    const pages = Math.floor(this.data.total / this.data.limit);
    const currentPage = Math.floor(this.data.offset / this.data.limit);
    const result = {
      prev: "",
      next: "",
      items: []
    };
    result.prev = (currentPage - 1) * this.data.limit;
    result.next = (currentPage + 1) * this.data.limit;
    let index = [currentPage - 1, currentPage, currentPage + 1];
    if (currentPage == 0) {
      result.prev = "";
      index = [currentPage, currentPage + 1, currentPage + 2];
    } else if (currentPage == pages) {
      result.next = "";
      index = [currentPage - 2, currentPage - 1, currentPage];
    }
    result.items = index.map(item => {
      return {
        title: item + 1,
        offset: item * this.data.limit,
        active: item == currentPage
      };
    });
    return result;
  }
  render() {
    this.parent.innerHTML = this.renderFn(this.calculateDataRender());
    this.initEvents();
  }
  setData(data) {
    if (data && data.limit !== undefined) {
      this.data.limit = data.limit;
    }
    if (data && data.offset !== undefined) {
      this.data.offset = data.offset;
    }
    if (data && data.total !== undefined) {
      this.data.total = data.total;
    }
    this.render();
  }
  initEvents() {
    $(this.parent)
      .find(".js-nav")
      .each((key, item) => {
        $(item).on("click", () => {
          if (item.dataset.page !== undefined) {
            this.data.setOffset(item.dataset.page);
          }
        });
      });
  }
}

class FlatItemList {
  constructor(data) {
    this.template = `<a class="filters-table__item" href="{{href}}">
      <div class="filters-table__col filters-table__col--number">
          <p class="filters-table__head">№</p>
          <p class="filters-table__text">{{number}}</p>
      </div>
      <div class="filters-table__col filters-table__col--corpus">
          <p class="filters-table__head">Корпус</p>
          <p class="filters-table__text">{{corpus}}</p>
      </div>
      <div class="filters-table__col filters-table__col--view">
          <p class="filters-table__head">Вид из окна</p>
          <p class="filters-table__text">{{view}}</p>
      </div>
      <div class="filters-table__col filters-table__col--area">
          <p class="filters-table__head">Площадь</p>
          <p class="filters-table__text">{{area}} м²</p>
      </div>
      <div class="filters-table__col filters-table__col--floor">
          <p class="filters-table__head">Этаж</p>
          <p class="filters-table__text">{{level}}</p>
      </div>
      <div class="filters-table__col filters-table__col--price">
          <p class="filters-table__head">Стоимость</p>
          <p class="filters-table__text">{{price}} ₽</p>
      </div>
      <div class="filters-table__col filters-table__col--discount">
        <%if (discount){%>
          <div class="filters-table__discount">
            <svg width="17" height="27">
              <use xlink:href="#discount"></use>
            </svg>
          </div>
        <%}%>
      </div>
    </a>`;
    this.data = data;
    this.renderFn = _.template(this.template);
  }
  render() {
    return this.renderFn(this.data);
  }
}
const itemListClasses = {
  TypeItemList,
  FlatItemList
};
class Filter {
  constructor(form, targetList, elViewCount, elSorted, elPaginations) {
    this.form = form;
    this.allRange = [];
    this.type = $(this.form).hasClass('filters__choice--type');
    if (this.form.dataset.listType) {
      this.listObject = itemListClasses[this.form.dataset.listType];
    } else {
      this.listObject = TypeItemList;
    }
    this.targetList = targetList;
    if (elViewCount) {
      this.elViewCount = elViewCount;
    }
    if (elSorted.length > 0) {
      this.elSorted = elSorted;
      this.initSort();
    }
    if (elPaginations) {
      this.elPaginations = elPaginations;
      this.pagination = new Paginations(elPaginations, {
        setOffset: this.setOffset.bind(this)
      });
    }
    this.debousedUpdate = _.debounce(this.updateFilter.bind(this), 500);
    const debousedUpdate = this.debousedUpdate;
    this.restoreStateFromURL();
    this.initVisualization();
    $(form.elements).on("change", this.debousedUpdate);
    this.updateFilter(false);
    this.initInputDisabled();

    const btnClear = this.form.querySelector('.filters__clear');

    if (btnClear) {
      btnClear.addEventListener('click', (event) => {
        event.preventDefault();
        const $inps = $(this.form.elements);

        this.allRange.forEach((el) => {
          if (!el.inputs[0].disabled) {
            const min = el.config.min;
            const max = el.config.max;

            el.refresh({ values: [+min, +max], });
          }
        });

        $inps.each((i, el) => {
          if (!el.disabled) {
            if (el.type && el.type === "checkbox") {
              el.checked = false;
            }

            $(el).change();
          }
        });
      });
    }
  }
  restoreStateFromURL() {
    let storedValues = [];
    let store;
    let $inps = $(this.form.elements);

    if (this.type) {
      store = localStorage.getItem('filter-data') || '';
      storedValues = parseQueryString(store);
    } else {
      storedValues = parseQueryString(window.location.search);
      if (Object.keys(storedValues).length === 0) {
        store = localStorage.getItem('filter-data') || '';
        storedValues = parseQueryString(store);
      }
    }
    for (let p in storedValues) {
      $inps.filter(`[name='${p}'], [name='${p}[]']`).each((index, el) => {
        if (el.type && (el.type === "checkbox" || el.type === "radio")) {
          let isChecked = true;

          if (_.isArray(storedValues[p])) {
            isChecked = _.includes(storedValues[p], el.value);
          }

          el.checked = isChecked;
        } else if (el.tagName === "SELECT") {
          if (storedValues[p]) {
            for (var i = 0; i < el.options.length; i++) {
              let opt = el.options[i];
              if (storedValues[p].includes(opt.value)) {
                opt.selected = "selected";
              }
            }
          }
        } else {
          el.value = storedValues[p];
        }
      });
    }
  }
  declOfNum(number, titles) {
    const cases = [2, 0, 1, 1, 1, 2];
    return titles[number % 100 > 4 && number % 100 < 20 ? 2 : cases[number % 10 < 5 ? number % 10 : 5]];
  }
  setOffset(num) {
    this.form.offset.value = num;
    this.debousedUpdate();
  }
  setSort(field, direction) {
    if (direction == "") {
      field = "";
    }
    this.form.sort.value = field;
    this.form.sortdir.value = direction;
    this.renderSort();
    this.debousedUpdate();
  }
  initVisualization() {
    const ranges = [].slice.call(this.form.querySelectorAll(".filters__range"));
    ranges.forEach(item => {
      const rangeFrom = item.querySelector(".filters__range-input--from");
      const rangeTo = item.querySelector(".filters__range-input--to");
      let range = null;
      if (!item.classList.contains("filters__range--price")) {
        range = new JSR([rangeFrom, rangeTo], {
          min: +rangeFrom.getAttribute("min"),
          max: +rangeFrom.getAttribute("max"),
          sliders: 2,
          values: [+rangeFrom.value, +rangeTo.value],
          labels: {
            minMax: true
          },
          grid: false
        });
      } else {
        const min = Math.floor(+rangeFrom.getAttribute("min") / 10000) * 10000;
        const max = Math.ceil(+rangeTo.getAttribute("max") / 10000) * 10000;
        rangeFrom.min = min;
        rangeFrom.max = max;
        rangeTo.min = min;
        rangeTo.max = max;
        range = new JSR([rangeFrom, rangeTo], {
          min,
          max,
          sliders: 2,
          values: [Math.floor(+rangeFrom.value / 10000) * 10000, Math.ceil(+rangeTo.value / 10000) * 10000],
          step: 10000,
          labels: {
            minMax: true,

            formatter(value) {
              const val = value / 1000000;
              return `${val.toString()} млн`;
            }

          },
          grid: false
        });
      }
      range.addEventListener("update", this.debousedUpdate);
      range.addEventListener("update", (input) => {
        const event = new CustomEvent('change', {
          cancelable: true,
        });

        input.dispatchEvent(event);
      });
      rangeFrom.addEventListener("change", () => {
        this.initChangeRange(rangeFrom, rangeFrom.parentNode);
      });
      rangeTo.addEventListener("change", () => {
        this.initChangeRange(rangeTo, rangeTo.parentNode);
      });
      this.initChangeRange(rangeFrom, rangeFrom.parentNode);
      this.initChangeRange(rangeTo, rangeTo.parentNode);

      this.allRange.push(range);
    });
  }
  initChangeRange(input, parent) {
    let val = +input.value;
    if (val >= 100000) {
      val = `${val / 1000000} млн`;
    }

    if (input.classList.contains("filters__range-input--from")) {
      parent.querySelector('.jsr_label.jsr_label--minmax[data-jsr-id="0"]').textContent = val;
    } else {
      parent.querySelector('.jsr_label.jsr_label--minmax[data-jsr-id="1"]').textContent = val;
    }
  }
  renderSort() {
    this.elSorted.children().removeClass("sorter-desc sorter-asc");
    const el = this.elSorted.find('[data-field="' + this.form.sort.value + '"]');
    if (this.form.sortdir.value == "desc") {
      el.addClass("sorter-desc");
    } else if (this.form.sortdir.value == "asc") {
      el.addClass("sorter-asc");
    }
  }
  initSort() {
    const sortsEl = this.elSorted.children();
    sortsEl.on("click", e => {
      const { field } = e.target.dataset;
      let dir = this.form.sortdir.value;
      if (this.form.sort.value == field) {
        dir = dir == "asc" ? "desc" : "";
      } else {
        dir = "asc";
      }
      this.setSort(field, dir);
    });
    this.renderSort();
  }
  initInputDisabled() {
    const items = [].slice.call(document.querySelectorAll('.filters__main-checkbox-input, .filters__checkbox-input, .filters__range-input'));
    items.forEach((item) => {
      if (item.disabled) {
        const parent = item.parentNode.parentNode.parentNode;
        if (!parent.classList.contains('filters__choice')) {
          parent.classList.add('filters__disabled');
        } else {
          item.parentNode.parentNode.classList.add('filters__disabled');
        }
      }
    });
  }
  updateFilter(setUrl = true) {
    $.ajax({
      url: this.form.action,
      method: this.form.method,
      dataType: "json",
      data: $(this.form).serialize(),
    })
      .done(data => {
        if (data.total == 0) {
          this.targetList.innerHTML = `
          <div class="filters-not-found__icon"><svg width="27" height="27"><use xlink:href="#search"></use></svg></div>
          <p class="filters-not-found__text">Планировки по заданным параметрам не найдены, пожалуйста, измените параметры поиска.</p>
        `;
          this.targetList.classList.add("filters-not-found");
          if (this.elPaginations) this.elPaginations.style.display = "none";
        } else {
          this.targetList.classList.remove("filters-not-found");
          if (this.elPaginations0) {
            this.elPaginations.style.display = null;
            this.pagination.setData({
              total: data.total,
              limit: data.limit,
              offset: data.offset
            });
          }
          const listTemplate = data.flats.map(item => new this.listObject(item)).reduce((summ, item) => {
            return summ + item.render();
          }, "");
          this.targetList.innerHTML = listTemplate;
        }
        if (this.elViewCount) {
          this.elViewCount.innerHTML =
            data.total + this.declOfNum(data.total, [" тип квартиры", " типа квартир", " типов квартир"]);
        }
        if (setUrl) {
          const myform = $(this.form);
          const disabled = myform.find(':input:disabled').removeAttr('disabled');
          const serialized = myform.serialize();
          const searcStr = myform
            .find("input, select")
            .not("[name='act']")
            .serialize();
          disabled.attr('disabled','disabled');
          localStorage.setItem('filter-data', searcStr);
          if (!this.type) {
            window.history.pushState({}, document.title, window.location.pathname + "?" + searcStr);
          }
        }
      })
      .fail(() => {
        alert("Не удалось получить данные с сервера!\nПопробуйте позже");
      });
  }
}

if (window.filterFlats && window.filterLists) {
  const params = [
    window.filterFlats,
    window.filterLists,
    $(".js-all-count")[0],
    $(".js-list-sort"),
    $(".js-paginations")[0]
  ];
  new Filter(...params);
}


loadFlatImg();

function loadFlatImg() {
  var loadImage = function(src, callback) {
    var img = new Image();
    img.onload = img.onerror = function(evt) {
      callback(evt.type == "error", img);
      img.onload = img.onerror = img = null;
    };
    img.src = src;
  };

  if ($(".flat__img").parents('.filters').length === 0) {
    loadImage($(".flat__img").attr("src"), function(err, img) {
      if (img) {
        const ratio = img.height / img.width;

        if (ratio > 0.55 && ratio < 1.5) $(".flat__img").addClass("flat__img--square");
        if (ratio > 1.5) $(".flat__img").addClass("flat__img--vertical");
      }
    });
  }
}
