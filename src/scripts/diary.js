const scrollBlock = document.querySelector('.diary_history');
const btnVideo = document.querySelector('.diary_video');
const btnOnline = document.querySelector('.diary_online');
const playVideo = document.querySelector('.popupVideo_wrapper');
const playOnline = document.querySelector('.popupOnline_wrapper');
const btnPrev = document.querySelector('.diary_btn-prev');
const btnNext = document.querySelector('.diary_btn-next');
let activeMonth = undefined;

const sliderConstruction = new Swiper('.diary_slider', {
    noSwipingClass: 'diary_slider',
    noSwiping: true,
    navigation: {
        nextEl: '.diary_btn-next',
        prevEl: '.diary_btn-prev',
    },
    pagination: {
        el: '.swiper-pagination',
        type: 'custom',
        renderCustom: function(sliderConstruction, current, total) {
            return `<span>${current}</span> из ${total}`;
        }
    }
});

if (sliderConstruction) addEventsForSlider();

function addEventsForSlider() {
  sliderConstruction.on('slideChange', () => {
    checkArrow();
  });

  function checkArrow() {
    const activeMonth = document.querySelector('.diary_item-active');

    if (activeMonth) {
      const activeMonthPrev = activeMonth.previousElementSibling;
      const activeMonthNext = activeMonth.nextElementSibling;

      if (activeMonthPrev === null && sliderConstruction.activeIndex === 0) {
        btnPrev.classList.add('diary_btn-disabled');
      } else {
        btnPrev.classList.remove('diary_btn-disabled');
      }

      if (sliderConstruction.activeIndex === sliderConstruction.slides.length - 1) {

        if (activeMonthNext === null) {
          btnNext.classList.add('diary_btn-disabled');
        } else if (activeMonthNext.classList.contains('diary_item')) {
          btnNext.classList.remove('diary_btn-disabled');
        } else {
          btnNext.classList.add('diary_btn-disabled');
        }
      } else {
        btnNext.classList.remove('diary_btn-disabled');
      }
    }
  }
}

const switchPreviousMonth = () => {
    activeMonth = document.querySelector('.diary_item-active');
    let activeMonthPrev = activeMonth.previousElementSibling;
    let url = null;

    if (activeMonthPrev !== null) {
        url = activeMonthPrev.getAttribute('href');
    }

    if (url !== null) {
        document.location.href = url;
    }
    // if (url === window.location.pathname) {
    //     document.location.href = `${window.location.origin}${window.location.pathname}`;
    // }
}

btnPrev.addEventListener('click', switchPreviousMonth);

sliderConstruction.on('slideNextTransitionEnd', () => {
    btnPrev.removeEventListener('click', switchPreviousMonth);
});

sliderConstruction.on('reachBeginning', () => {
    activeMonth = document.querySelector('.diary_item-active');
    let activeMonthPrev = activeMonth.previousElementSibling;
    let url = null;

    if (activeMonthPrev !== null) {
        url = activeMonthPrev.getAttribute('href');
    }

    btnPrev.onclick = () => {
        if (url !== null) {
            document.location.href = url;
        }
        // if (url === window.location.pathname) {
        //     document.location.href = `${window.location.origin}${window.location.pathname}`;
        // }
    }
});

sliderConstruction.on('reachEnd', () => {
    activeMonth = document.querySelector('.diary_item-active');
    let activeMonthNext = activeMonth.nextElementSibling;
    let url = activeMonthNext.getAttribute('href');

    btnNext.onclick = () => {
        if (url !== null) {
            document.location.href = url;
        }
    }
});

let tag = document.createElement('script');
tag.src = "https://www.youtube.com/iframe_api";
let firstScriptTag = document.getElementsByTagName('script')[0];
firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

function onPlayerStateChange(event) {
    if (event.data === YT.PlayerState.ENDED) {
        playVideo.classList.remove('popupVideo_wrapper-play');
    }
};

const activePopupVideo = (el, elClass, poster) => {
    if (poster.classList.contains('popupVideo_wrapper-play')) {
        poster.classList.remove('popupVideo_wrapper-play');
    }

    el.classList.add(elClass);
};

const autoPlayVideo = (el, btn, id) => {
    const player = new YT.Player(id, {
        height: '100%',
        width: '100%',
        videoId: btn.getAttribute('data-videoid'),
        events: {
            'onStateChange': onPlayerStateChange
        }
    });

    el.addEventListener('click', () => {
        el.classList.add('popupVideo_wrapper-play');
        setTimeout(() => {
            if (player) player.playVideo();
        }, 100);
    });
}

if (btnVideo) {
  btnVideo.addEventListener('click', () => {
    activePopupVideo(btnVideo, 'diary_video-active', playVideo);
    autoPlayVideo(playVideo, btnVideo, 'player');
  });
}

if (btnOnline) {
  btnOnline.addEventListener('click', () => {
    activePopupVideo(btnOnline, 'diary_online-active', playOnline);
    autoPlayVideo(playOnline, btnOnline, 'playerCamera');
  });
}

const ps = new PerfectScrollbar(scrollBlock);
setTimeout(() => {
  checkScrollNav();
}, 0);
checkArrow();

function checkScrollNav() {
  if (window.innerWidth < 1024) {
    var left = $('.diary_item-active').offset().left;

    if (left > 52) scrollBlock.scrollLeft = left + 4;
  }
}
