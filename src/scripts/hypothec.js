const wrapperListBank = document.querySelector('.hypothec_banksWrapper');
const banks = [].slice.call(document.querySelectorAll('.hypothec_bank'));
const sortingBtn = [].slice.call(document.querySelectorAll('.hypothec_btn'));
let banksAttr = [].slice.call(document.querySelectorAll('.hypothec_bank[data-src]'));
let banksPopups = [].slice.call(document.querySelectorAll('.popup'));

const sorting = (arr, value) => {
  arr.sort((a, b) => {
    if (wrapperListBank.classList.contains('hypothec_banksWrapper-ascending') === true) {
      return Number(Number(a.getAttribute(value))) - Number(b.getAttribute(value))
    } else {
      return Number(Number(b.getAttribute(value)) - Number(a.getAttribute(value)))
    }
  });

  for (let i = 0; i < arr.length; i++) {
    wrapperListBank.appendChild(arr[i]);
  }
}

sortingBtn.forEach((el) => {
  el.addEventListener("click", () => {
    if (el.classList.contains('hypothec_btn-active') !== true) {
      for (let i = 0; i < sortingBtn.length; i++) {
        if (sortingBtn[i].classList.contains('hypothec_btn-active') === true) {
          sortingBtn[i].classList.remove('hypothec_btn-active')
          sortingBtn[i].classList.remove('hypothec_btn-ascending')
          sortingBtn[i].classList.remove('hypothec_btn-descending')
        }
      }

      el.classList.add('hypothec_btn-active');
    }

    if (el.classList.contains('hypothec_btn-ascending') === false) {
      el.classList.add('hypothec_btn-ascending');
      el.classList.remove('hypothec_btn-descending');
      wrapperListBank.classList.add('hypothec_banksWrapper-ascending')
    } else if (el.classList.contains('hypothec_btn-ascending') === true) {
      el.classList.remove('hypothec_btn-ascending');
      el.classList.add('hypothec_btn-descending');
      wrapperListBank.classList.remove('hypothec_banksWrapper-ascending')
    }

    if (el.classList.contains("hypothec_btnContribution")) {
      sorting (banks, "data-contribution")
    } else if (el.classList.contains("hypothec_btnRate")) {
      sorting (banks, "data-rate")
    } else if (el.classList.contains("hypothec_btnTerm")) {
      console.log(banks)
      sorting (banks, "data-term")
    }
  });
});

const hiddenDescription = (el) => {
  el.classList.remove('hypothec_bank-active');
  el.style.maxHeight = null
}

banksAttr.forEach((element) => {
  element.addEventListener('click', () => {
    if (document.body.offsetWidth >= 768) {
      for (let i = 0; i < banksPopups.length; i++) {
        if (element.getAttribute("data-src") === banksPopups[i].getAttribute("id")) {
          $.fancybox.open(banksPopups[i])
        }
      }
    } else {
      if (element.classList.contains('hypothec_bank-active') === true) {
        hiddenDescription(element);
      } else {
        for (let i = 0; i < banksAttr.length; i++) {
          if (banksAttr[i].classList.contains('hypothec_bank-active') === true) {
            hiddenDescription(banksAttr[i]);
          }
        }

        element.classList.add('hypothec_bank-active');
        element.style.maxHeight = `${element.scrollHeight}px`
      }
    }
  });
});

window.addEventListener('resize', () => {
  if (document.body.offsetWidth > 768) {
    for (let i = 0; i < banksAttr.length; i++) {
      if (banksAttr[i].classList.contains('hypothec_bank-active')) {
        hiddenDescription(banksAttr[i])
      }
    }
  } else {
    let popupOpen = document.querySelector('.popup.fancybox-content');
    $.fancybox.close(popupOpen);
  }

  checkGrayHeight();
});


function checkGrayHeight() {
  let topCall = $('.hypothec .callMe').position().top;
  let indent = 108;

  if (topCall === 0) topCall = $('.hypothec_container--main .form_success').position().top;

  if (window.innerWidth < 768) {
    indent = 90;
  } else if (window.innerWidth >= 768 && window.innerWidth < 1024) {
    indent = 84;
  }
  const top = topCall - indent;

  $('.hypothec__gray').css({'top': top + 'px', 'height': '300vh'});
}

checkGrayHeight();
setTimeout(() => {
  if (window.innerWidth < 768) checkGrayHeight();
}, 100);
