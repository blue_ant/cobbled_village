const scrollContentDetail = document.querySelector('.stock_detail');
const scrollContentList = document.querySelector('.stock_list');

const liststock = new PerfectScrollbar(scrollContentList);
const detailstock = new PerfectScrollbar(scrollContentDetail);

const stockList = [].slice.call(document.querySelectorAll('.stock_item'));
const stockDetail = [].slice.call(document.querySelectorAll('.stock_detailItem'));
const stockDetailPopup = [].slice.call(document.querySelectorAll('.popupStock'));

const titlestock = [].slice.call(document.querySelectorAll('.stock_headline'));
const descstock = [].slice.call(document.querySelectorAll('.stock_text'));

const lengthStr = (str, numLength) => {
    let stocktr = '';

    if (str.length > numLength) {
        return stocktr = `${str.substring(0, numLength)}...`;
    }

    return str;
}

titlestock.forEach(el => {
    let str = el.innerHTML;
    el.innerHTML = lengthStr(str, 50);
});

descstock.forEach(el => {
    let str = el.innerHTML;
    el.innerHTML = lengthStr(str, 180);
});

const searhDetail = (el) => {
    for (let i = 0; i < stockDetail.length; i++) {
        if (el.getAttribute('data-open') === stockDetail[i].getAttribute('data-open')) {
            stockDetail[i].classList.add('stock_detailItem-active');
        }
    }
}

stockList.forEach((element) => {
    if (element.classList.contains('stock_item-active')) {
        searhDetail(element);
    }

    element.addEventListener('click', () => {
        if (elHTML.clientWidth < 1024) {
            for (let i = 0; i < stockDetailPopup.length; i++) {
                if (element.getAttribute('data-src') === stockDetailPopup[i].getAttribute('id')) {
                    $.fancybox.open(stockDetailPopup[i])
                }
            }
        }

        for (let i = 0; i < stockList.length; i++) {
            if (stockList[i].classList.contains('stock_item-active')) {
                stockList[i].classList.remove('stock_item-active');
            }
        }

        for (let i = 0; i < stockDetail.length; i++) {
            if (stockDetail[i].classList.contains('stock_detailItem-active')) {
                stockDetail[i].classList.remove('stock_detailItem-active');
            }
        }

        element.classList.add('stock_item-active');
        searhDetail(element);
    });

    window.addEventListener('resize', () => {
        if (elHTML.clientWidth > 1024) {
            for (let i = 0; i < stockDetailPopup.length; i++) {
                if (stockDetailPopup[i].classList.contains('fancybox-content')) {
                    $.fancybox.close(stockDetailPopup[i]);
                }
            }
        }
    });
});
