house();

function house() {
  var loadImage = function(src, callback) {
    var img = new Image();
    img.onload = img.onerror = function(evt) {
      callback(evt.type == 'error', img);
      img.onload = img.onerror = img = null;
    };
    img.src = src;
  };

  $('.house__visual-floor').click(function() {
    if ($(window).width() >= 1280 || $(this).parent().hasClass('house__visual-house--active')) {
      var link = $(this).data('url');
      window.location = link;
    }
  });

  $('.house__visual-floor').hover(function(e) {
    setTimeout(() => {
      var section = $(this).data('section');
      var floor = $(this).data('floor');

      $('.house__label-text--section span').text(section);
      $('.house__label-text--floor span').text(floor);

      $('.house__label').addClass('house__label--active');
      $(this).parent().addClass('house__visual-house--active');
    }, 1);
  }, function() {
    $(this).parent().removeClass('house__visual-house--active');
    $('.house__label').removeClass('house__label--active');
  });

  $('.house__content').mousemove(function(e){
      var windowWidth = $(window).width();
      var scrollLeft = $('.house__content-wrap').scrollLeft();
      var pos = $(this).offset();
      var elem_left = pos.left;
      var elem_top = pos.top;
      var minTop = -(elem_top - $('.house').offset().top);
      var widthLabel = $('.house__label').outerWidth();
      var x = e.pageX - elem_left + 26;
      var y = e.pageY - elem_top - 46;

      if (minTop > y) y = (minTop + 10);
      if (windowWidth + scrollLeft < widthLabel + x - 42) x = (x - widthLabel - 42);

      $('.house__label').css({'top': y + 'px', 'left': x + 'px'});
  });

  loadImage($('.house__img').attr('src'), function(err, img) {
    if (err) {
      alert('image not load');
    } else {
      var ratio = img.width / img.height;
      var windowWidth = $(window).width();

      updateGenplan(ratio, windowWidth);

      $(window).resize(() => {
        windowWidth = $(window).width();
        updateGenplan(ratio, windowWidth);
      });
    }
  });

  function updateGenplan(ratio, windowWidth) {
    $('.house__content').css({'height': '', 'margin-top': '', 'margin-bottom': '', 'margin-left': ''});
    $('.house__img').css({'height': '', 'width': ''});

    resizeImg(ratio, windowWidth);
  }

  function resizeImg(ratio, windowWidth) {
    const $genplan = $('.house');
    const $contentWrap = $genplan.find('.house__content-wrap');
    const $content = $genplan.find('.house__content');
    const $img = $genplan.find('.house__img');
    let $hGenplan = $genplan.outerHeight();
    let $hContentWrap = $contentWrap.outerHeight();

    if ($hGenplan > $hContentWrap) {
      $content.css({'margin-top': '0', 'margin-bottom': '0'});

      $hGenplan = $genplan.outerHeight();
      $hContentWrap = $contentWrap.outerHeight();


      if ($hGenplan > $hContentWrap) {
        const imgWidth = $hGenplan * ratio;
        const genWidth = $genplan.width();
        $content.css({'height': `${$hGenplan}px`});
        $img.css({'height': '100%', 'width': `${imgWidth}px`});


        if (windowWidth >= 768) {
          $content.css({'margin-left': `${-(imgWidth - genWidth) * 0.25}px`});
        } else {
          $content.css({'margin-left': ''});
        }
      }
    }
  }
}
