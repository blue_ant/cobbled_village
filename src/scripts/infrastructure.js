/*=require0 ./includes/blocks/Map.js*/
const mapContainer = document.getElementById('map');

let map = new Map('infrastructureMap', 15, mapContainer, 'data-markers');
map.switchDisplayMarkers();
map.filtration();
