const btnAddres = document.querySelectorAll('.transport_accessibility_item');
const tabsTransport = document.querySelectorAll('.transport_accessibility_tab');
const btnAddresActive = document.querySelector('.transport_accessibility_item-active');
const car = document.getElementById('car');
const buses = document.getElementById('buses');
const map = document.getElementById('map');
let road = undefined;
let moscow_map = undefined;

const activeClass = (arr, classActive, element) => {
    for (let i = 0; i < arr.length; i++) {
        if (arr[i].classList.contains(classActive)) {
            arr[i].classList.remove(classActive);
        }
    }

    element.classList.add(classActive);
}

const routeSelection = () => {
    const btnAddresActive = document.querySelector('.transport_accessibility_item-active');

    if (buses.classList.contains('transport_accessibility_tab-active')) {
        road = route(btnAddresActive.getAttribute('data-lan'), btnAddresActive.getAttribute('data-lon'), 'masstransit');
    } else if (car.classList.contains('transport_accessibility_tab-active')) {
        road = route(btnAddresActive.getAttribute('data-lan'), btnAddresActive.getAttribute('data-lon'), 'auto');
    }
};

const setCenterMap = () => {
  if (window.innerWidth >= 768) {
    setTimeout(() => {
      moscow_map.setCenter([55.834809, 37.317328]);
    }, 1000);
  }
};

const route = (lan, lon, transport) => {
    let multiRoute = new ymaps.multiRouter.MultiRoute({
        referencePoints: [
            [lan, lon],
            [55.834809, 37.317328],
        ],
        params: {
            routingMode: transport
        }
    }, {
        wayPointVisible: false,
        routeActiveStrokeColor: "#29ca51",
        boundsAutoApply: true,
        pinVisible: false,
        routeStrokeColor: "#e23333",
        routeActivePedestrianSegmentStrokeColor: "#e23333",
    });
    return multiRoute;
};

const initMap = () => {
  moscow_map = new ymaps.Map("map", {
    center: [55.826253, 37.320746],
    zoom: 13,
    controls: []
  });

  moscow_map.behaviors.disable(['scrollZoom'])

  const zoomControl = new ymaps.control.ZoomControl({
    options: {
      size: "small",
      position: {
        top: 10,
        right: 10,
        left: "auto"
      }
    }
  });

  const fullScreenControl = new ymaps.control.FullscreenControl({
    options: {
      position: {
        top: 80,
        right: 10,
      }
    }
  });

  moscow_map.controls
  .add(zoomControl)
  .add(fullScreenControl);

  ymaps.option.presetStorage
  .add('icon#metro', {
    iconLayout: ymaps.templateLayoutFactory.createClass("<div class='transport_accessibility_icon transport_accessibility_icon-metroMap'><svg width='14' height='10'><use xlink:href='#metro_2'></use></svg></div>"),
  })
  .add('icon#mkad', {
    iconLayout: ymaps.templateLayoutFactory.createClass("<div class='transport_accessibility_icon transport_accessibility_icon-mkadMap'><svg width='13' height='18'><use xlink:href='#mkad'></use></svg></div>")
  })
  .add('icon#train', {
    iconLayout: ymaps.templateLayoutFactory.createClass("<div class='transport_accessibility_icon transport_accessibility_icon-trainMap'><svg width='14' height='14'><use xlink:href='#train'></use></svg></div>")
  });

  let krasnogors = new ymaps.Placemark(
    [55.835027, 37.317152], {}, {
      iconLayout: 'default#image',
      iconImageHref: 'https://bruschaty.ru/img/icon_map/home.png',
      iconImageSize: [60, 74]
    }
  )

  let objectManager = new ymaps.ObjectManager({});

  road = route(btnAddresActive.getAttribute('data-lan'), btnAddresActive.getAttribute('data-lon'), 'auto');

  btnAddres.forEach((el) => {
    el.addEventListener("click", () => {
      activeClass(btnAddres, 'transport_accessibility_item-active', el);

      moscow_map.geoObjects.remove(road);
      routeSelection();
      moscow_map.geoObjects.add(road);
      setCenterMap();
    });
  });

  tabsTransport.forEach((el) => {
    el.addEventListener('click', () => {
      activeClass(tabsTransport, 'transport_accessibility_tab-active', el);

      moscow_map.geoObjects.remove(road);
      routeSelection();
      moscow_map.geoObjects.add(road);
      setCenterMap();
    });
  });

  moscow_map.geoObjects
  .add(objectManager)
  .add(krasnogors)
  .add(road);
  setCenterMap();

  $.ajax({
    url: `${map.getAttribute('data-json')}`
  }).done(function(data) {
    objectManager.add(data);
  });

  ymapsTouchScroll(moscow_map);
};

ymaps.ready(initMap);
